using System;
using System.IO.Ports;
using System.Drawing;
using System.Windows.Forms;

public class HelloWorld : Form
{
    private System.Windows.Forms.FlowLayoutPanel FlowLayoutPanel1;
    private System.Windows.Forms.FlowLayoutPanel FlowLayoutPanel2;
    private System.Windows.Forms.TextBox textBox1;

    static public void Main ()
    {
        Application.Run (new HelloWorld ());
    }

    static public string[] GetPorts () {
        Console.WriteLine ("Hello Mono World");
        string[] ports = SerialPort.GetPortNames();

        return ports;
    }

    public HelloWorld ()
    {
        string[] ports = GetPorts();
        this.FlowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
        this.FlowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
        //this.FlowLayoutPanel2.AutoSize = true;
        Console.WriteLine("The following serial ports were found:");
        foreach(string port in ports) {
            Console.WriteLine(port);
            Button b = new Button ();
            b.Text = port;
	    this.FlowLayoutPanel1.Controls.Add(b);
	}
        Button b1 = new Button ();
        b1.Text = "/dev/pts/0";

	this.FlowLayoutPanel1.Controls.Add(b1);
        //this.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
        this.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;

	this.textBox1 = new System.Windows.Forms.TextBox();
        this.textBox1.AcceptsReturn = true;
        this.textBox1.AcceptsTab = true;
        //this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.textBox1.Multiline = true;
        //this.textBox1.AutoSize = true;
        this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.textBox1.Width = 500;
        this.textBox1.Height = 500;
	this.Text = "TextBox Example";
	Controls.Add(textBox1);
	//this.FlowLayoutPanel2.Controls.Add(textBox1);
	//this.FlowLayoutPanel2.MinimumSize = new Size(100, 1000);
	//this.FlowLayoutPanel1.MinimumSize = new Size(100, 1000);

	Controls.Add(FlowLayoutPanel1);
	Controls.Add(FlowLayoutPanel2);
	textBox1.Location = new Point(200, 0);
        // b.Click += new EventHandler (Button_Click);
        // Controls.Add (b);
    }

    /*
    private void Button_Click (object sender, EventArgs e)
    {
        MessageBox.Show ("Button Clicked!");
    }
    */
}
